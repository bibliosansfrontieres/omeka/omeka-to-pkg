This provides a command-line tool to build media content packages for the
Ideascube mediacenter, from a file containing the required metadata.

Those files are now created by Omeka, the content management system used by
Libraries Without Borders.

It was originally written by Matthieu Gautier as a part of
[Ideascube](https://github.com/ideascube/ideascube) itself, but was later on
extracted to live its own life.

# Quickstart

In a Python 3 virtual environment:

    $ pip install git+https://framagit.org/ideascube/omeka-to-pkg.git
    $ omeka-to-pkg medias-to-import.csv generated-package-file

For more information see:

    $ omeka-to-pkg --help
