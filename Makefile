# -- Development --------------------------------------------------------------
develop:
	pip3 install --no-use-wheel -r requirements-dev.txt

upgrade-deps:
	pip-compile --rebuild --header --index --annotate --upgrade requirements.in
	pip-compile --rebuild --header --index --annotate --upgrade requirements-dev.in

sync-deps:
	pip-sync requirements-dev.txt

ci-images:
	docker build --no-cache --file dockerfiles/ci-jessie-python34 --tag ideascube/omekatopkg-ci:jessie-python34 .
	docker push ideascube/omekatopkg-ci:jessie-python34

	docker build --no-cache --file dockerfiles/ci-stretch-python35 --tag ideascube/omekatopkg-ci:stretch-python35 .
	docker push ideascube/omekatopkg-ci:stretch-python35


# -- Testing ------------------------------------------------------------------
test:
	py.test

test-coverage:
	py.test --cov=omekatopkg --cov-report=term-missing --cov-fail-under=98

quality-check:
	py.test --flakes --mccabe --pep8 -m 'flakes or mccabe or pep8'
