import pytest


@pytest.mark.parametrize(
    'content, expected',
    [
        ('', 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855'),  # noqa
        ('foobar', 'c3ab8ff13720e8ad9047dd39466b3c8974e592c2fa383d4a3960714caef0c4f2'),  # noqa
        ('a' * 10000000, '01f4a87c04b40af59aadc0e812293509709c9a8763a60b7f9e19303322f8b03c'),  # noqa
    ],
    ids=['empty-file', 'foobar', 'big-file'])
def test_get_file_sha256(tmpdir, content, expected):
    from omekatopkg.utils import get_file_sha256

    path = tmpdir.join('file')
    path.write_text(content, 'utf-8')

    assert get_file_sha256(path.strpath) == expected


@pytest.mark.parametrize(
    'content, expected',
    [
        ('', 0),
        ('foobar', 6),
    ],
    ids=['empty-file', 'foobar'])
def test_file_size(tmpdir, content, expected):
    from omekatopkg.utils import get_file_size

    path = tmpdir.join('file')
    path.write_text(content, 'utf-8')

    assert get_file_size(path.strpath) == expected
